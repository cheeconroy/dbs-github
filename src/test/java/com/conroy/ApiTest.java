package com.conroy;

import com.conroy.entity.Account;
import com.conroy.entity.Repo;
import com.conroy.repository.AccountRepository;
import com.conroy.service.GithubDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiTest {

    GithubDao githubDao = new GithubDao();

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AccountRepository accountRepository;

    @BeforeEach
    void beforeTest(){
        System.out.println("Running github api test");
    }

    @Test
    @DisplayName("Simple github account run check")
    void testUser(){
        Mono<Account> githubAccount = githubDao.getGithubAccount("conroychee");
//        int id = githubAccount.block().getId();

        githubAccount.subscribe(x->assertEquals(38206057, x.getId()));
//        assertEquals(38206057, id);
    }


    @Test
    @DisplayName("Github repo run check")
    void testRepo(){
        Mono<Repo> repoMono = githubDao.getUserRepo("conroychee", "antd-theme-switch");
        repoMono.subscribe(x->assertEquals(301643091, x.getId()));
    }


    @Test
    @DisplayName("Github account api run check. It checks whether the id of conroychee is retrieved correctly")
    void testUserApi() throws Exception {
        String url = String.format("http://localhost:%s/user/conroychee", port);
        Account account = restTemplate.getForObject(url, Account.class);
        assertEquals(38206057, account.getId());
    }

    @Test
    @DisplayName("Github account api run check. It checks whether the repo id of conroychee and antd-theme-swtich is retrieved correctly")
    void testRepoApi() throws Exception {
        String url = String.format("http://localhost:%s/repositories/conroychee/antd-theme-switch", port);
        Account githubRepo = restTemplate.getForObject(url, Account.class);
        assertEquals(301643091, githubRepo.getId());
    }

    @Test
    @DisplayName("Github account api run check. It checks whether the id of conroychee is retrieved correctly")
    void testUserDbInsertion() throws Exception {
        String url = String.format("http://localhost:%s/user/conroychee", port);
        Account account = restTemplate.getForObject(url, Account.class);
        accountRepository.save(account);
    }

    @Test
    @DisplayName("Github account api run check. It checks whether the id of conroychee is retrieved correctly")
    void testUserDbRetrieval() throws Exception {
        Mono<Account> account = accountRepository.findByLogin("conroychee");
        account.subscribe(x->assertEquals("conroychee", x.getLogin()));
    }
}
