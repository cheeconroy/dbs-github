package com.conroy.router;

import com.conroy.entity.Account;
import com.conroy.handler.Handler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springdoc.core.annotations.RouterOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class RouterPut {
    @Autowired
    private Handler handler;

    @Bean
    @RouterOperation(operation = @Operation(operationId = "storeGithubAccount", summary = "Store Github account to db", tags = {"Database Github"},
            parameters = {@Parameter(in = ParameterIn.PATH, name = "login", description = "Github username")},
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Account.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> storeGithubAccount() {
        return RouterFunctions.route()
                .PUT("/storeGithubAccount/{login}", handler::storeGithubAccount)
                .build();
    }

    @Bean
    @RouterOperation(operation = @Operation(operationId = "storeGithubRepo", summary = "Store Github repo to db", tags = {"Database Github"},
            parameters = {@Parameter(in = ParameterIn.PATH, name = "login", description = "Github username"), @Parameter(in = ParameterIn.PATH, name = "repoName", description = "Github repo name")},
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Account.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> storeGithubRepo() {
        return RouterFunctions.route()
                .PUT("/storeGithubRepo/{login}/{repoName}", handler::storeGithubRepo)
                .build();
    }
}
