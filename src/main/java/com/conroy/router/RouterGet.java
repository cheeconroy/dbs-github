package com.conroy.router;

import com.conroy.entity.Account;
import com.conroy.entity.Repo;
import com.conroy.handler.Handler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springdoc.core.annotations.RouterOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;


@Configuration
public class RouterGet {
    @Autowired
    private Handler handler;

    @Bean
    @RouterOperation(operation = @Operation(operationId = "getGithubAccountByLogin", summary = "Get Github account details by github login", tags = {"Github"},
            parameters = {@Parameter(in = ParameterIn.PATH, name = "login", description = "Github username")},
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Account.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> getGithubAccount()  {
        return RouterFunctions.route()
                .GET("/user/{login}", handler::getGithubAccount)
                .build();

    }

    @Bean
    @RouterOperation(operation = @Operation(operationId = "getGithubRepoByRepoAndLogin", summary = "Get Github repo details by github login and repo name", tags = {"Github"},
            parameters = {@Parameter(in = ParameterIn.PATH, name = "login", description = "Github username"), @Parameter(in = ParameterIn.PATH, name = "repoName", description = "Github repo name")},
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Repo.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username or repo"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> getGithubRepo() {
        return RouterFunctions.route()
                .GET("/repositories/{login}/{repoName}", handler::getGithubRepo)
                .build();

    }

    @Bean
    @RouterOperation(operation = @Operation(operationId = "getDbGithubAccountByLogin", summary = "Get github account which stored in Cloud MySql", tags = {"Database Github"},
            parameters = {@Parameter(in = ParameterIn.PATH, name = "login", description = "Github account in Mysql")},
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Account.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username or repo"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> getDbGithubAccount() {
        return RouterFunctions.route()
                .GET("/db/user/{login}", handler::getDbGithubAccount)
                .build();

    }

    @Bean
    @RouterOperation(operation = @Operation(operationId = "getDbGithubAccountByLogin", summary = "Get github account which stored in Cloud MySql", tags = {"Database Github"},
            requestBody =             @RequestBody(description = "Created user object", required = true,
                    content = @Content(
                            schema = @Schema(implementation = String.class))),
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = String.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username or repo"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> getDbGithubAccountList() {
        return RouterFunctions.route()
                .POST("/db/userList", handler::getDbGithubAccountList)
                .build();
    }

    @Bean
    @RouterOperation(operation = @Operation(operationId = "getDbGithubRepoByRepoAndLogin", summary = "Get github account which stored in Cloud MySql", tags = {"Database Github"},
            parameters = {@Parameter(in = ParameterIn.PATH, name = "login", description = "Github account in Mysql"), @Parameter(in = ParameterIn.PATH, name = "repoName", description = "Github repo name in Mysql")},
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Repo.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username or repo"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> getDbGithubRepo() {
        return RouterFunctions.route()
                .GET("/db/repositories/{login}/{repoName}", handler::getDbGithubRepo)
                .build();

    }


    @Bean
    @RouterOperation(operation = @Operation(operationId = "getDbGithubRepoListByLogin", summary = "Get github repository list owned by a user stored in Cloud MySql", tags = {"Database Github"},
            parameters = {@Parameter(in = ParameterIn.PATH, name = "login", description = "Github repository list in Mysql")},
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Repo.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username or repo"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> getDbGithubRepoList() {
        return RouterFunctions.route()
                .GET("/db/repositoryList/{login}", handler::getDbGithubRepoList)
                .build();

    }



    @Bean
    @RouterOperation(operation = @Operation(operationId = "getGithubAccountListByLogins", summary = "Get github account from github API", tags = {"Github"},
            requestBody =             @RequestBody(description = "Created user object", required = true,
                    content = @Content(
                            schema = @Schema(implementation = String.class))),
            responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = String.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid github username or repo"),
                    @ApiResponse(responseCode = "404", description = "Github username not found")}))
    public RouterFunction<ServerResponse> getGithubAccountList() {
        return RouterFunctions.route()
                .POST("/userList", handler::getGithubAccountList)
                .build();
    }

}
