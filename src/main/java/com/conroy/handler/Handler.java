package com.conroy.handler;

import com.conroy.entity.Repo;
import com.conroy.handler.subscriber.EndSubscriber;
import com.conroy.repository.AccountRepository;
import com.conroy.service.GithubDao;
import com.conroy.entity.Account;
import com.conroy.service.SqlDao;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


@EntityScan("com.conroy.entity")
@Service
public class Handler {

    @Autowired
    private EndSubscriber<Repo> endRepoSubscriber;

    @Autowired
    private EndSubscriber<Account> endAccountSubscriber;

    @Autowired
    private GithubDao githubDao;

    @Autowired
    private SqlDao sqlDao;

    RestTemplate restTemplate = new RestTemplate();

    /**
     * Get the github account from github api call
     *
     * @param request
     * @return
     */
    public Mono<ServerResponse> getGithubAccount(ServerRequest request) {
        String login = request.pathVariable("login");
        Mono<Account> githubAccount = githubDao.getGithubAccount(login);
        githubAccount.subscribe(endAccountSubscriber);
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(githubAccount, Account.class);
    }

    public Mono<ServerResponse> getGithubAccountList(ServerRequest request) {
        Flux<Account> results = request.bodyToMono(String.class)
                .flatMapMany(x->{
                    JSONObject req = new JSONObject(x);
                    List loginList = req.getJSONArray("logins").toList();
                    return githubDao.getGithubAccountList(loginList);
                });
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(results, Account.class);
    }


    /**
     * Get the github repo from github api call
     *
     * @param request
     * @return
     */
    public Mono<ServerResponse> getGithubRepo(ServerRequest request) {
        String login = request.pathVariable("login");
        String repoName = request.pathVariable("repoName");
        Mono<Repo> githubrepo = githubDao.getUserRepo(login, repoName);
        githubrepo.subscribe(endRepoSubscriber);
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(githubrepo, Repo.class);
    }


    /**
     * Store the github account to Mysql
     *
     * @param request
     * @return
     */
    public Mono<ServerResponse> storeGithubAccount(ServerRequest request) {
        String login = request.pathVariable("login");
        Mono<Account> account = sqlDao.storeGithubUser(login);
        account.subscribe(endAccountSubscriber);
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(account, Account.class);
    }


    /**
     * Get the github account directly from Gcloud Mysql
     *
     * @param request
     * @return
     */
    public Mono<ServerResponse> getDbGithubAccount(ServerRequest request) {
        String login = request.pathVariable("login");
        Mono<Account> account = sqlDao.getAccount(login);
        account.subscribe(endAccountSubscriber);
        return ServerResponse.ok().body(account, Account.class);
    }

    /**
     * Get db account list from Gcloud mysql
     * @param request
     * @return
     */
    public Mono<ServerResponse> getDbGithubAccountList(ServerRequest request) {
        Flux<Account> results = request.bodyToMono(String.class)
                .flatMapMany(x->{
                    JSONObject req = new JSONObject(x);
                    List loginList = req.getJSONArray("logins").toList();
                    return sqlDao.getAccountList(loginList);
                });
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(results, Account.class);

    }

    /**
     * Get the github repo from db
     * @param request
     * @return
     */
    public Mono<ServerResponse> getDbGithubRepo(ServerRequest request) {
        String login = request.pathVariable("login");
        String repoName = request.pathVariable("repoName");
        Mono<Repo> repo = sqlDao.getRepo(login, repoName);
        repo.subscribe(endRepoSubscriber);
        return ServerResponse.ok().body(repo, Repo.class);
    }

    /**
     * Get db repo list for a github user
     * @param request
     * @return
     */
    public Mono<ServerResponse> getDbGithubRepoList(ServerRequest request) {
        String login = request.pathVariable("login");
        Flux<Repo> repoList = sqlDao.getRepoList(login);
        repoList.subscribe(endRepoSubscriber);
        return ServerResponse.ok().body(repoList, Repo.class);
    }


    /**
     * Store github repo into db
     * @param request
     * @return
     */
    public Mono<ServerResponse> storeGithubRepo(ServerRequest request) {
        String login = request.pathVariable("login");
        String repoName = request.pathVariable("repoName");
        Mono<Repo> repo = sqlDao.storeGithubRepo(login, repoName);
        repo.subscribe(endRepoSubscriber);
        return ServerResponse.ok().body(repo, Repo.class);
    }


}



