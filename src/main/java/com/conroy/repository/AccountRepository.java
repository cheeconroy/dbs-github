package com.conroy.repository;

import com.conroy.entity.Account;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Set;


@Repository
public interface AccountRepository extends GenericRepository<Account> {
    Mono<Account> findByLogin(String login);
    Flux<Account> findByLoginIn(List logins);
}