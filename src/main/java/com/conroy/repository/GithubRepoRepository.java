package com.conroy.repository;

import com.conroy.entity.Repo;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


@Repository
public interface GithubRepoRepository extends GenericRepository<Repo> {
    Flux<Repo> findByLogin(String login);
    Mono<Repo> findByLoginAndName(String login, String name);

}
