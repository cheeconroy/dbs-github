package com.conroy.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Set;


@NoRepositoryBean
public interface GenericRepository<T> extends  ReactiveCrudRepository<T, Integer> {

}
