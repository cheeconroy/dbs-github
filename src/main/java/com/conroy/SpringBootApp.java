package com.conroy;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@ComponentScan("com.conroy")
@SpringBootApplication
@OpenAPIDefinition
@EntityScan("com.conroy.entity")
@EnableR2dbcRepositories("com.conroy.repository")
public class SpringBootApp {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootApp.class, args);
    }

}
