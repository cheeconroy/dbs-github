package com.conroy.service;

import com.conroy.entity.Account;
import com.conroy.entity.Repo;
import com.conroy.repository.AccountRepository;
import com.conroy.repository.GithubRepoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Service
public class SqlDao {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    GithubRepoRepository githubRepoRepository;

    @Autowired
    GithubDao githubDao;

    public Mono<Account> getAccount(String login){
        System.out.println("Get login: " + login);
        Mono<Account> account = accountRepository.findByLogin(login);
        return account;
    }

    public Mono<Repo> getRepo(String login, String repoName){
        Mono<Repo> repo = githubRepoRepository.findByLoginAndName(login, repoName);
        return repo;
    }

    public Flux<Repo> getRepoList(String login){
        Flux<Repo> repoList = githubRepoRepository.findByLogin(login);

        return repoList;
    }

    public Flux<Account> getAccountList(List logins){
        Flux<Account> accountList = accountRepository.findByLoginIn(logins);
        return accountList;
    }

    /*

    DB storing

     */

    public Mono<Account> storeGithubUser(String login)  {
        return Mono.fromFuture(CompletableFuture.supplyAsync(() -> githubDao.getGithubAccount(login))
                .thenApply(x -> {
                    if (x == null) {
                        System.out.println("Entity not found");
                    }
                    return x;
                })
                .handle((result, ex) -> {
                    if (null != ex) {
                        ex.printStackTrace();
                        return null;
                    } else {
                        System.out.println("HANDLING " + result);
                        return result;
                    }
                })
                .thenApply(x -> {
                    Account account = x.block();
                    if (accountRepository.findById(Objects.requireNonNull(Objects.requireNonNull(account).getId())).block() == null) { //if account exists in db as its id found
                        account.setNewAccount(true);
                    }
                    assert account != null;
                    accountRepository.save(account).subscribe();
                    return account;
                })

        );
    }

    public Mono<Repo> storeGithubRepo(String login, String repoName){
        return Mono.fromFuture(CompletableFuture.supplyAsync(() -> githubDao.getUserRepo(login, repoName))
                .thenApply(x -> {
                    if (x == null) {
                        System.out.println("Entity not found");
                    }
                    return x;
                })
                .handle((result, ex) -> {
                    if (null != ex) {
                        ex.printStackTrace();
                        return null;
                    } else {
                        System.out.println("HANDLING " + result);
                        return result;
                    }
                })
                .thenApply(x -> {
                    Repo repo = x.block();
                    if (githubRepoRepository.findById(Objects.requireNonNull(Objects.requireNonNull(repo).getId())).block() == null) { //if account exists in db as its id found
                        repo.setNewRepo(true);
                    }
                    assert repo != null;
                    githubRepoRepository.save(repo).subscribe();
                    return repo;
                })

        );
    }
}
