package com.conroy.service;

import com.conroy.entity.Account;
import com.conroy.entity.Repo;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SynchronousSink;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;


@Service
public class GithubDao {

    RestTemplate restTemplate = new RestTemplate();

    public Mono<Account> getGithubAccount(String account) {

        String url = String.format("https://api.github.com/users/%s", account);

        return Mono.fromFuture(CompletableFuture.supplyAsync(() -> restTemplate.getForObject(url, Account.class))
                .handle((result, ex) -> {
                    if (null != ex) {
                        throw new ResponseStatusException(
                                HttpStatus.NOT_FOUND, "entity not found"
                        );
                    } else {
                        return result;
                    }
                }));
    }

    public Mono<Repo> getUserRepo(String login, String repoName) {
        String url = String.format("https://api.github.com/repos/%s/%s", login, repoName);

        return Mono.fromFuture(CompletableFuture.supplyAsync(() -> restTemplate.getForObject(url, Repo.class))
                .handle((result, ex) -> {
                    if (null != ex) {
                        throw new ResponseStatusException(
                                HttpStatus.NOT_FOUND, "entity not found"
                        );
                    } else {
                        String ownerLogin = result.getFullName().replaceAll("/.*", "");
                        result.setLogin(ownerLogin);
                        return result;
                    }
                }));
    }

    public Flux<Account> getGithubAccountList(List loginList){
        Callable<Integer> initialState = () -> 0;
        BiFunction<Integer, SynchronousSink<Account>, Integer> generator = (state, sink) -> {
            String login = loginList.get(state).toString();

            String url = String.format("https://api.github.com/users/%s", login);
            Account value = new Account();
            value.setLogin("Unfound user");
            try {
                value = restTemplate.getForObject(url, Account.class);
            } catch (HttpClientErrorException ex) {
                System.out.println(String.format("Account %s not found", login));
            }
            sink.next(value);
            if (state == loginList.size() - 1) {
                sink.complete();
            }
            return state + 1;
        };
        Flux<Account> accountFlux = Flux.generate(initialState, generator);
        return accountFlux;

    }

}
