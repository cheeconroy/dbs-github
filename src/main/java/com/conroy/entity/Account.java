package com.conroy.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;


@JsonInclude(JsonInclude.Include.NON_NULL) // ignore those null in parameter
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Persistable<Integer> {

    @Id
    private Integer id;

    private String login;
    private String type;
    private String name;

    @JsonProperty("node_id")
    private String nodeId;


    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("updated_at")
    private String updatedAt;

    @Transient //ignore this column, not persistent
    private boolean newAccount;

    @Override
    @Transient
    public boolean isNew() { // to identify whether it is new row or not
        return this.newAccount;
    }

}
