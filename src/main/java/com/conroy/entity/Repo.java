package com.conroy.entity;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;

@JsonInclude(JsonInclude.Include.NON_NULL) // ignore those null in parameter
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Repo implements Persistable<Integer> {

    @Id
    private Integer id;

    private String login;

    @JsonProperty("node_id")
    private String nodeId;

    private String name;

    @JsonProperty("full_name")
    private String fullName;

    @JsonProperty("html_url")
    private String htmlUrl;

    private String description;
    private String fork;
    private String url;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("updated_at")
    private String updatedAt;

    @Transient //ignore this column, not persistent
    private boolean newRepo;

    @Override
    @Transient
    public boolean isNew() { // to identify whether it is new row or not
        return this.newRepo;
    }

}
