# Web-flux-demo-dbs
## _The webflux with github_



The web-flux-demo-dbs is a cloud-enabled, backend which queries the github repository and store the data into gcloud db. It has OpenAPI for API discovery. This is the assignment from DBS.

It has the following feature
- Springboot webflux
- Bitbucket CI/CD pipeline for testing, build and deployment to GCP
- Open API enabled for API discovery
